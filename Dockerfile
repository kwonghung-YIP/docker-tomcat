ARG JDK_IMG_TAG
FROM kwonghung/docker-jdk:${JDK_IMG_TAG}

ARG TOMCAT_MAJOR
ARG TOMCAT_VERSION
ENV CATALINA_HOME=/usr/local/tomcat

RUN mkdir -p /usr/local/tomcat/download;

RUN apt-get update; \
	apt-get install -y wget gnupg; \
	set -eux; \
    FILE_SUFFIX=apache-tomcat-$TOMCAT_VERSION; \
	FILE_GZ=$FILE_SUFFIX.tar.gz; \
	FILE_ASC=$FILE_GZ.asc; \
	FILE_SHA512=$FILE_GZ.sha512; \
	URL_GZ_SUFFIX=tomcat/tomcat-$TOMCAT_MAJOR/v$TOMCAT_VERSION/bin/$FILE_GZ; \
	URL_GZ=http://apache.01link.hk/$URL_GZ_SUFFIX; \
	URL_GZ=http://ftp.cuhk.edu.hk/pub/packages/apache.org/$URL_GZ_SUFFIX; \
	URL_GZ=https://archive.apache.org/dist/$URL_GZ_SUFFIX; \	
	URL_KEYS=https://archive.apache.org/dist/tomcat/tomcat-$TOMCAT_MAJOR/KEYS; \
	URL_ASC=https://archive.apache.org/dist/tomcat/tomcat-$TOMCAT_MAJOR/v$TOMCAT_VERSION/bin/$FILE_ASC; \
	URL_SHA512=https://archive.apache.org/dist/tomcat/tomcat-$TOMCAT_MAJOR/v$TOMCAT_VERSION/bin/$FILE_SHA512; \
	cd /usr/local/tomcat/download; \
	wget $URL_GZ; \
	wget $URL_KEYS; \
	wget $URL_ASC; \
	wget $URL_SHA512; \
	sha512sum -c $FILE_SHA512; \	
	gpg --import KEYS; \
	gpg --verify $FILE_ASC; \
	tar -xvf $FILE_GZ; \
	cd ..; \
	mv download/$FILE_SUFFIX/* .; \
	rm -rf download; \
	pwd && ls -l; \
	apt-get purge -y --auto-remove wget; \
    apt-get clean;

RUN apt-get install -y libapr1-dev libssl-dev gcc make; \
	cd $CATALINA_HOME/bin; \
	tar -xvf tomcat-native.tar.gz; \
	cd tomcat-native-*-src/native; \
	./configure --with-ssl=yes; \
	make && make install; \
	cd $CATALINA_HOME/bin; \
	rm $CATALINA_HOME/bin/tomcat-native.tar.gz; \
	rm -rf $CATALINA_HOME/bin/tomcat-native-*-src; \
	apt-get purge -y --auto-remove libapr1-dev libssl-dev gcc make; 

ENV CATALINA_BASE=/usr/local/catalina_base

RUN mkdir -p /usr/local/catalina_base/bin; \    
    mkdir -p /usr/local/catalina_base/conf; \
	mkdir -p /usr/local/catalina_base/lib; \
	mkdir -p /usr/local/catalina_base/logs; \
	mkdir -p /usr/local/catalina_base/webapps; \
	mkdir -p /usr/local/catalina_base/work; \
	touch /usr/local/catalina_base/bin/setenv.sh; \	
	cp -r /usr/local/tomcat/conf/* /usr/local/catalina_base/conf; \
	chmod -R +x /usr/local/tomcat/bin/*.sh; \
	chmod -R +x /usr/local/catalina_base/bin/*.sh;
    

#gpg --keyserver www.apache.org

#tar -xvf apache-tomcat-8.5.23.tar.gz --strip-components=1

ENV PATH=$PATH:$CATALINA_HOME/bin
WORKDIR /usr/local/catalina_base

EXPOSE 8080

CMD ["catalina.sh","run"]